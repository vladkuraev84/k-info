var doc = $(document), win = $(window), hammer = $(document).hammer();
win.on({
	orientationchange: function(){
		locationIMG();
		show_popup();
	},
	load: function(){
		contentLoaded();
		pageReady();
	},
	touchmove: function () {
		$('.touch').removeClass('touch')
	},
	touchend: function () {
		$('.touch').removeClass('touch')
	}
});
var src_location;
function contentLoaded() {
	src_location = $('.location').find('img').attr('src');
	$('.popup.center').appendTo('#section');
	current_link();
	locationIMG();
	add_pull_block();
}
////////// TOUCH ACTION //////////
elTap('a, :submit, :button, .main_menu li, .static_menu li, [data-sidebar], .btn, .btn-icon, [data-popup], .featureditem li, .featureapage, .relatedpages li, .listpages li, .listcategories li, .sitemap li, .listxrss li, .search-results li, .news-list-item, .img_gallery li');
function elTap(elTouch) {
	hammer.on('touch', elTouch,function (e) {
		$(this).addClass('touch');
	}).on('tap', elTouch, function (e) {
		var page = $(this).data('page');
		if (page) {
			hide_popup(e);
			$('[data-page]').removeClass('current')
			$(this).addClass('current');
			$('.page').removeClass('active');
			$("#" + page).addClass('active');
			$("#section").animate({ scrollTop: 0 }, 0);
			hide_sidebar();
		} else if ($(this).find('.page-title').length) {
			window.location.href = $(this).find('.page-title').attr('href');
		}
		e.gesture.stopDetect();
	});
}
hammer.on('touch', '.up, [data-sidebar], [data-popup], [data-page], #container.left, #container.right', function (e) {
	e.gesture.preventDefault();
	return false;
})//stops the default action
	.on('touch', '#section.wrap, [data-popup].current', hide_popup)
	.on('tap', '[data-popup]', switch_popup)
	.on('tap', '[data-sidebar], #container.left, #container.right', switch_sidebar)
	.on('tap', '.tabs li', tabs)
	.on('tap', '.up', scroll_up)
	.on('dragdown dragup release', '.pullHorizontal', pull_action)
	.on('dragdown release', '.pullDown', pull_action)
	.on('dragup release', '.pullUp', pull_action);

//////////end TOUCH ACTION //////////

///////// BUTTON UP SCROLL /////////
function scroll_up(e) {
	$("#section").animate({ scrollTop: 0 });
}
/////////end BUTTON UP SCROLL /////////

///////// SHOW\HIDE sidebar /////////
var sidebarSwipe;
function switch_sidebar(e) {
	e.gesture.preventDefault();
	var sidebar = $(this).data('sidebar');
	if ($('#container').hasClass('left') || $('#container').hasClass('right')) {
		hide_sidebar();
	}
	show_sidebar(sidebar, sidebar == 'left' ? 'right' : 'left');
	hide_popup(e);
}
//Hide sidebar
function hide_sidebar() {
	$('#container').removeClass('left right');
	$('[data-sidebar]').removeClass('current');
	setTimeout(function () {
		$('.sidebar').removeClass('active');
	}, 200);
	sidebarSwipe = '';
}
//Show sidebar
function show_sidebar(direction, swipe) {
	$('.sidebar.' + direction).addClass('active');
	$('#container').addClass(direction);
	$('[data-sidebar="' + direction + '"]').addClass('current');
	sidebarSwipe = swipe;
}
/////////end SHOW\HIDE sidebar /////////

///////// SHOW\HIDE POPUP /////////
var popupName = null;
function switch_popup(e) {
	hide_popup(e);
	popupName = $(this).data('popup');
	show_popup(e);
}
function hide_popup(e) {
	if ($(e.target).hasClass('popup center') || $(e.target).closest('.popup.center').length) {
		return
	}
	e.gesture.preventDefault();
	popupName = null;
	$('[data-popup]').removeClass('current');
	$('.popup').hide();
	$('#section').removeClass('wrap')
	e.gesture.stopDetect();
}
function show_popup(e) {
	$('#' + popupName).show();
	var heightP = $('#' + popupName).outerHeight();
	$('#' + popupName + '.center').css({ 'margin-top': $('#section').scrollTop() - heightP / 2 });
	$('[data-popup="' + popupName + '"]').addClass('current');
	$('#section').addClass('wrap');
}
/////////end SHOW\HIDE POPUP /////////

///////// CURRENT LINK /////////
function current_link() {
	var currentUrl = decodeURI(window.location.href);
	if (currentUrl && typeof currentUrl != 'undefined') {
		$("a[href='" + currentUrl + "']").addClass('current');
	}
}
/////////end CURRENT LINK /////////

///////// TABS /////////
function tabs() {
	var index = $(this).index();
	var tabName = $(this).parent().data('tab');
	$(this).parent().children().removeClass('active');
	$('#' + tabName).children().css({
		'-webkit-transform': 'translateX(' + -100 * index + '%)',
		'-moz-transform': 'translateX(' + -100 * index + '%)',
		'-o-transform': 'translateX(' + -100 * index + '%)',
		'transform': 'translateX(' + -100 * index + '%)'
	}, 300);
	$(this).addClass('active');
}
/////////end TABS /////////

///////// CLIENT LOCATION for GET DIRECTION /////////
function locationIMG() {
	var width = $(window).innerWidth();
	var height = $(window).innerWidth() > $(window).innerHeight() ? $(window).innerWidth() / 2 : $(window).innerWidth();
	$('.location').find('img').attr({
		'src': src_location.replace('SIZEIMG', width + 'x' + height)
	});
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (position) {
			var pos = new google.maps.LatLng(position.coords.latitude,
				position.coords.longitude);
			geocoder(pos.lat(), pos.lng())
		});
	}
}
function geocoder(lat, lng) {
	$.ajax({
		url: 'http://maps.google.com/maps/api/geocode/json?address=' + lat + ',' + lng + '&sensor=false',
		dataType: 'json',
		success: function (results) {
			var full_address = results.results[0].formatted_address;
			$('[name="saddr"]').val(full_address);
		}
	});
}
/////////end CLIENT LOCATION for GET DIRECTION /////////
///////// PULL TO REFRESH /////////
var oldHTML, dragSwitcher = false,
	codeTop = '<div id="pull-down" class="pull"><span class="icon-arrow-down icon24"></span> <span class="pull-info">pull to load...</span></div>',
	codeBottom = '<div id="pull-up" class="pull"><span class="icon-arrow-up icon24"></span> <span class="pull-info">pull to load...</span></div>',
	codeLoad = '<span class="icon-loading rotate animated icon24"></span> <span class="pull-info">loading...</span>'
function add_pull_block(){
	if($('.pullHorizontal, .pullDown, .pullUp').length != 0){
		$('#section').before(codeTop).after(codeBottom);
	}
}
function pull_action(e){
	switch(e.type) {
		case 'dragup':
			if($('#section').scrollTop() !=  $('#section').prop('scrollHeight')-$('#section').outerHeight()){ e.gesture.stopDetect(); break; }
		case 'dragdown':
			if($('#section').scrollTop() !=  0 && e.gesture.direction == 'down'){ e.gesture.stopDetect(); break; }
			dragSwitcher = true;
			e.gesture.preventDefault();
			show_pull_block('#pull-'+ e.gesture.direction, Math.abs(e.gesture.deltaY));
			if(Math.abs(e.gesture.deltaY) > 50){
				$('.pull').addClass('release').find('.pull-info').text('release to load...');
			}else{
				$('.pull').removeClass('release').find('.pull-info').text('pull to load...');
			}
			break;
		case 'release':
			if(!dragSwitcher){ break; }
			dragSwitcher = false
			if(Math.abs(e.gesture.deltaY) > 50 && e.gesture.direction == 'down'){
				$('#pull-down').removeAttr('style').addClass('loading').html(codeLoad);
				oldHTML = $('#section').html();
				pullDownAction($(this));
				DOM_change('#pull-down', codeTop);
			}else if(e.gesture.deltaY < -50 && e.gesture.direction == 'up'){
				$('#pull-up').removeAttr('style').addClass('loading').html(codeLoad);
				oldHTML = $('#section').html();
				pullUpAction($(this));
				DOM_change('#pull-up', codeBottom);
			}else{
				$('.pull').removeAttr('style')
			}
			$('.pull').removeClass('release');
			break;
	}
}
function show_pull_block(el, distance){
	$(el).css({
		'height': distance+'px',
		'line-height': distance+'px'
	});
	if(el == '#pull-up'){ $('#section').animate({ scrollTop:1000000 },0); }
}
function DOM_change(directionPull, codePull){
	var changeHTML = setInterval(function () {
		if ($('#section').html() != oldHTML) {
			$(directionPull).removeClass('loading').replaceWith(codePull);
			clearInterval(changeHTML);
		}
	}, 100);
	setTimeout(function() { $(directionPull).removeClass('loading').replaceWith(codePull); }, 3000);
}
/////////end PULL TO REFRESH /////////

///////// SLIDER ON FULL SCREEN /////////
var slider, slide, slide_title, slide_length = 0, slide_width = 100, current_slide = 0, pos_slide = 0, imageWidth, imgWidthNow, imgWidthLast;
doc.hammer({swipe_velocity: 0.2, drag_lock_to_axis: true })
	.on("tap", '#sliderSwipe .close', sliderSwipe_hide)
	.on("tap", 'a.gall, a._lbox', sliderSwipe_show)
	.on("release dragleft dragright swipeleft swiperight", '#carouselSwipe', sliderSwipe);

function sliderSwipe_show(e) {
	e.gesture.preventDefault();
	$('body').append(
		'<div id="sliderSwipe">' +
			'<div id="carouselSwipe">' +
				'<ul></ul>' +
			'</div>' +
			'<div id="titleSwipe">' +
				'<span class="icon-cancel icon24 close btn-icon"></span>' +
				'<span class="title"></span>' +
			'</div>' +
		'</div>'
	)
	$('#section').addClass('wrap');
	slider = $('#carouselSwipe>*');
	slide_title = $('#titleSwipe .title');
	var gal = $(this).attr('rel');
	if (gal) {
		var current = $(this).attr('href')
		$('[rel="' + gal + '"]').each(function () {
			var href = $(this).attr('href'),
				title = $(this).attr('title');
			slider.append('<li><img src="' + href + '" alt="' + title + '"></li>');
		});
		slide = slider.find('>*');
		slide_length = slide.length;
		slide_width = 100 / slide_length;
		slider.width(100 * slide_length + '%');
		slide.width(slide_width + '%');
		current_slide = slide.has('[src="' + current + '"]').index();
		pos_slide = -current_slide * slide_width;
		movingX(pos_slide);
	} else {
		var href = $(this).attr('href'),
			title = $(this).attr('title');
		slider.append('<li><img src="' + href + '" alt="' + title + '"></li>');
		slide = slider.find('>*');
		slide_title.text(slide.eq(current_slide).find('img').attr('alt'))
	}
}
function sliderSwipe_hide(e) {
	e.gesture.preventDefault();
	$('#sliderSwipe').remove();
	$('#section').removeClass('wrap');
	slide_length = 0, slide_width = 100, current_slide = 0, pos_slide = 0;
}
var switcher = true;
function sliderSwipe(e) {
	if (!switcher) {
		return
	}
	e.gesture.preventDefault();
	switch (e.type) {
		case 'dragright':
		case 'dragleft':
			var drag_offset = e.gesture.deltaX,
				pos_drag = pos_slide / slide_width * slide.width()
			slider.addClass('move').css({
				'-webkit-transform': 'translateX(' + (pos_drag + drag_offset) + 'px)',
				'-moz-transform': 'translateX(' + (pos_drag + drag_offset) + 'px)',
				'-o-transform': 'translateX(' + (pos_drag + drag_offset) + 'px)',
				'transform': 'translateX(' + (pos_drag + drag_offset) + 'px)'
			})
			break;
		case 'swipeleft':
		case 'swiperight':
			step_direction(e.gesture.direction)
			e.gesture.stopDetect();
			break;
		case 'release':
			if (Math.abs(e.gesture.deltaX) > slide.width() / 2) {
				step_direction(e.gesture.direction)
			}
			else {
				movingX(pos_slide);
			}
			break;
	}
}
function step_direction(direction) {
	if (direction == 'right' && 0 >= pos_slide + slide_width) {
		current_slide--
		movingX(pos_slide + slide_width);
	} else if (direction == 'left' && pos_slide > -(slide_width * slide_length - slide_width)) {
		current_slide++
		movingX(pos_slide - slide_width);
	} else {
		movingX(pos_slide);
	}
}
function movingX(position) {
	pos_slide = position;
	slider.removeClass('move').css({
		'-webkit-transform': 'translateX(' + position + '%)',
		'-moz-transform': 'translateX(' + position + '%)',
		'-o-transform': 'translateX(' + position + '%)',
		'transform': 'translateX(' + position + '%)'
	});
	slide_title.text(slide.eq(current_slide).find('img').attr('alt'));
	imageWidth = slide.eq(current_slide).find('img').width();
}
doc.hammer({ transform_always_block: true }).on('doubletap transform transformstart transformend', '#carouselSwipe li', pinch);
function pinch(e) {
	e.gesture.preventDefault();
	switch (e.type) {
		case 'doubletap':
			imageWidth = slide.eq(current_slide).find('img').width();
			$(this).find('img').removeAttr('style').toggleClass('fullWidth');
			switcher = !switcher ? true : false;
			e.gesture.stopDetect();
			break;
		case 'transformstart':
			switcher = false;
			$(this).find('img').addClass('fullWidth');
			imgWidthLast = $(this).find('img').width();
			break;
		case 'transform':
			imgWidthNow = imgWidthLast * e.gesture.scale
			$(this).find('img').width(imgWidthNow);
			break;
		case 'transformend':
			if (imageWidth > imgWidthNow) {
				$(this).find('img').removeAttr('style').removeClass('fullWidth');
				switcher = true;
			}
			if (imageWidth * 3 < imgWidthNow) {
				$(this).find('img').width(imageWidth * 3);
			}
			imgWidthLast = imgWidthNow;
			break;
	}
}