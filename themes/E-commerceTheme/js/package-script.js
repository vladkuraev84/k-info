$(document).ready(function () {

	$(document)
		.on('click', '.add-package', function(){
			if(!$(this).closest('.list-package').hasClass('selected')){
				$(this)
					.text('Remove').toggleClass('add-package remove-package')
					.closest('.list-package').addClass('selected').end()
					.closest('.product-item').clone().css('width','0').appendTo('.selected-box');
				var sku = $(this).siblings('.sku').text();
				var val = $('.selected-products .option-productpackage [type="text"]').val();
				$('.selected-products .option-productpackage [type="text"]').val(val + sku  + ', ');
				if($('.selected-box .product-item').length == $('.list-package:not(:empty())').length){
					$('.selected-products .action-link').removeClass('close');
				}
			}
		})
		.on('click', '.remove-package', function(){
			var sku = $(this).siblings('.sku').text();
			console.log(sku);
			var val = $('.selected-products .option-productpackage [type="text"]').val();
			$('.selected-products .product-item[data-sku="'+sku+'"]').remove();
			$('.list-package  .product-item[data-sku="'+sku+'"] .remove-package').text('Add to cart +').toggleClass('add-package remove-package').closest('.selected').removeClass('selected');
			$('.selected-products .option-productpackage [type="text"]').val(val.replace(sku + ', ', ""));
				if($('.selected-box .product-item').length != $('.list-package:not(:empty())').length){
					$('.selected-products .action-link').addClass('close');
				}
		}).on('click', '.remove-all', function(){
			$('.selected-products .option-productpackage [type="text"]').val('');
			$('.selected-products .selected-box').empty();
			$('.list-package .remove-package').text('Add to cart +').toggleClass('add-package remove-package').closest('.selected').removeClass('selected');
			$('.selected-products .action-link').addClass('close');
		});

	$('.selected-products .action-link .tcart-add').click(function (e) {
		if ($('.selected-box .product-item').length < $('.list-package:not(:empty())').length) {
			e.preventDefault();
			return false;
		}
	})

});