/*
 $(window).load(function(){
 $("ul").carousel({
 size: 2,
 speed: 300,
 visible: 7,
 vertical: true or false,
 elScroll: tag or class element,
 scrolling: true or false,
 scrollTime: 7000,
 responsiveMargin: margin for one side in px,
 btnPrev: html code or text for button prev,
 btnNext: html code or text for button next
 });
 });
 */
(function ($) {
	$.fn.carousel = function (o) {
		o = $.extend({
			size: 1,
			speed: 200,
			visible: 5,
			vertical: false,
			elScroll: false,
			scrolling: false,
			scrollTime: 7000,
			responsiveMargin: 10,
			btnPrev: true,
			btnNext: true
		}, o || {});
		return this.each(function () {

			var ul = $(this), li = o.elScroll ? $(this).find(o.elScroll) : ul.children(), liLength = li.length, v = o.visible, size = o.size > (liLength - v) ? liLength - v : o.size;

			//Code for the wrapper
			li.wrapAll(
				'<div class="carousel-clip" style="overflow: hidden;height: 100%;">' +
					'<div class="carousel-wrap"></div>' +
					'</div>'
			);
			if (v < liLength) {
				if (o.btnPrev && o.btnNext) {
					ul.append($('<a>').addClass("prev carousel-btn").attr({href: "javascript:;", title: "Prev", onselectstart: "return false", unselectable: "on"}).html(typeof (o.btnPrev) != "boolean" ? o.btnPrev : ""));
					ul.append($('<a>').addClass("next carousel-btn").attr({href: "javascript:;", title: "Next", onselectstart: "return false", unselectable: "on"}).html(typeof (o.btnNext) != "boolean" ? o.btnNext : ""));
					$('.carousel-clip, .carousel-btn', this).css({
						'-moz-user-select': 'none',
						'-khtml-user-select': 'none',
						'-webkit-user-select': 'none',
						'-o-user-select': 'none',
						'user-select': 'none'
					});
				}
			}

			var div = $('.carousel-wrap', this),
				ulHeight = ul.height(), liVarticalMargin = li.css('margin-top').replace('px', '') * 2,
				liH = ulHeight / v - liVarticalMargin, liFullHeight = ulHeight / v;

			ul.css({
				'overflow': 'hidden',
				'height': 'auto'
			});
			$(this).find('.carousel-clip').css({
				'height': o.vertical ? ulHeight : ''
			});
			div.css({
				'overflow': 'hidden',
				'margin': '0',
				'padding': '0',
				'width': o.vertical ? '100%' : 100 * liLength / v + '%',
				'height': o.vertical ? ulHeight * liLength / v : ''
			})
			o.responsiveMargin = o.responsiveMargin / ul.width() * 100 * liLength / v;
			li.css({
				'width': o.vertical ? '100%' : 100 / liLength - o.responsiveMargin + '%',
				'height': o.vertical ? liH : '',
				'float': "left",
				'margin-left': o.vertical ? '' : o.responsiveMargin / 2 + '%',
				'margin-right': o.vertical ? '' : o.responsiveMargin / 2 + '%'
			});

			if (!o.vertical) {
				// Prev button
				$('.prev', this).click(function () {
					clearInterval(horizontalScrolling);
					prevHorizontal();
					if( o.scrolling){
						setTimeout(
							function(){
								horizontalScrolling = setInterval(nextHorizontal, o.scrollTime)
							}, o.speed
						);
					}
				});
				// Next button
				$('.next', this).click(function () {
					clearInterval(horizontalScrolling);
					nextHorizontal();
					if( o.scrolling){
						setTimeout(
							function(){
								horizontalScrolling = setInterval(nextHorizontal, o.scrollTime)
							}, o.speed
						);
					}
				});
				var horizontalScrolling = o.scrolling || !o.btnPrev || !o.btnNext ? setInterval(nextHorizontal, o.scrollTime) : '';
			}
			else {
				// Prev button
				$('.prev', this).click(function () {
					clearInterval(verticalScrolling);
					prevVertical();
					if( o.scrolling){
						setTimeout(
							function(){
								verticalScrolling = setInterval(nextVertical, o.scrollTime)
							}, o.speed
						);
					}
				});
				// Next button
				$('.next', this).click(function () {
					clearInterval(verticalScrolling);
					nextVertical();
					if( o.scrolling){
						setTimeout(
							function(){
								verticalScrolling = setInterval(nextVertical, o.scrollTime)
							}, o.speed
						);
					}
				});
				var verticalScrolling = o.scrolling || !o.btnPrev || !o.btnNext ? setInterval(nextVertical, o.scrollTime) : '';
			}
			;

			// Prev button horizontal
			function prevHorizontal() {
				div.children(':gt(' + (liLength - size - 1) + ')').prependTo(div);
				div.stop(false, false).css({"margin-left": -100 * size / v + '%'}).animate({'marginLeft': "0px"}, o.speed);
			}

			// Next button horizontal
			function nextHorizontal() {
				div.stop(true, true).animate({'marginLeft': -100 * size / v + '%'}, o.speed, function () {
					div.children(':lt(' + size + ')').appendTo(div);
					div.css({"margin-left": "0px"});
				});
			}

			// Prev button vertical
			function prevVertical() {
				div.children(':gt(' + (liLength - size - 1) + ')').prependTo(div);
				div.stop(false, false).css({"margin-top": -liFullHeight * size + 'px'}).animate({'marginTop': "0px"}, o.speed);
			}

			// Next button vertical
			function nextVertical() {
				div.stop(true, true).animate({'marginTop': -liFullHeight * size + 'px'}, o.speed, function () {
					div.children(':lt(' + size + ')').appendTo(div);
					div.css({"margin-top": "0px"});
				});
			}
		});
	};
})(jQuery);
