$(document).ready(function(){


// variables

var gmat = "wood",
    roof = "majestic",
    deckmat = "wood",
    gbasemat = "charc",
    deck = 30,
    basesize = "large",
    posts = "straight",
    braces = "standard",
    railings = "standard",
    ezebreeze = "ez",
    gsize, gbase, cornice, floorboard, shingles, electrical, screenpackage, pricebase, pricematerial;

var finalprice = {
        f_gazebo: 0,
        f_roof: 0,
        f_shingles: 0,
        f_cornice: 0,
        f_posts: 0,
        f_braces: 0,
        f_railings: 0,
        f_color: 0,
        f_deck: 0,
        f_screen: 0,
        f_electrical: 0,
        f_color: 0,
        getPrice: function(){
            var sum = 0;
            for(prop in this){
                if(!isNaN(this[prop]))
                    sum += parseFloat(this[prop]);
                }
            return sum;
        }
}

var price = {
        octagon: {
            wood: {
                "8 ft": {
                    gazebo: 3000,
                    compositedeck: 960,
                    wooddeck: 795,
                    pagodaroof: "N/A",
                    paintedwhite: 865,
                    woodfinish: 540,
                    screenpackage: 930,
                    ezebreeze: 2185,
                    turnedposts: 295,
                    turnedspindles: 350,
                    wavedcornice: 195,
                    cedarshingles: Math.round(8*8*3.95)
                },
                "10 ft": {
                    gazebo: 3400,
                    compositedeck: 1395,
                    wooddeck: 1140,
                    pagodaroof: 725,
                    paintedwhite: 1100,
                    woodfinish: 760,
                    screenpackage: 1085,
                    ezebreeze: 2685,
                    turnedposts: 295,
                    turnedspindles: 455,
                    wavedcornice: 235,
                    cedarshingles: Math.round(10*10*3.95)
                },
                "12 ft": {
                    gazebo: 4200,
                    compositedeck: 2050,
                    wooddeck: 1560,
                    pagodaroof: 850,
                    paintedwhite: 1300,
                    woodfinish: 995,
                    screenpackage: 1295,
                    ezebreeze: 3285,
                    turnedposts: 295,
                    turnedspindles: 580,
                    wavedcornice: 295,
                    cedarshingles: Math.round(12*12*3.95)
                },
                "14 ft": {
                    gazebo: 5100,
                    compositedeck: 2800,
                    wooddeck: 2095,
                    pagodaroof: 1025,
                    paintedwhite: 1625,
                    woodfinish: 1200,
                    screenpackage: 1520,
                    ezebreeze: 3885,
                    turnedposts: 295,
                    turnedspindles: 730,
                    wavedcornice: 350,
                    cedarshingles: Math.round(14*14*3.95)
                },
                "16 ft": {
                    gazebo: 6400,
                    compositedeck: 3670,
                    wooddeck: 2760,
                    pagodaroof: 1200,
                    paintedwhite: 2060,
                    woodfinish: 1435,
                    screenpackage: 1895,
                    ezebreeze: 4585,
                    turnedposts: 495,
                    turnedspindles: 920,
                    wavedcornice: 465,
                    cedarshingles: Math.round(16*16*3.95)
                },
                "18 ft": {
                    gazebo: 7900,
                    compositedeck: 4530,
                    wooddeck: 3500,
                    pagodaroof: 1425,
                    paintedwhite: 2490,
                    woodfinish: 1660,
                    screenpackage: 2375,
                    ezebreeze: 5485,
                    turnedposts: 495,
                    turnedspindles: 1140,
                    wavedcornice: 580,
                    cedarshingles: Math.round(18*18*3.95)
                },
                "20 ft": {
                    gazebo: 10300,
                    compositedeck: 5615,
                    wooddeck: 4280,
                    pagodaroof: 1775,
                    paintedwhite: 2925,
                    woodfinish: 1875,
                    screenpackage: 2850,
                    ezebreeze: 6385,
                    turnedposts: 495,
                    turnedspindles: 1350,
                    wavedcornice: 695,
                    cedarshingles: Math.round(20*20*3.95)
                },
                "24 ft": {
                    gazebo: 15900,
                    compositedeck: 8100,
                    wooddeck: 6225,
                    pagodaroof: 2325,
                    paintedwhite: 3770,
                    woodfinish: 2270,
                    screenpackage: 3850,
                    ezebreeze: 7385,
                    turnedposts: 495,
                    turnedspindles: 1570,
                    wavedcornice: 940,
                    cedarshingles: Math.round(24*24*3.95)
                },
                "28 ft": {
                    gazebo: 20600,
                    compositedeck: 11120,
                    wooddeck: 8600,
                    pagodaroof: 3050,
                    paintedwhite: 4820,
                    woodfinish: 2725,
                    screenpackage: 5185,
                    ezebreeze: 8385,
                    turnedposts: 695,
                    turnedspindles: 1850,
                    wavedcornice: 1150,
                    cedarshingles: Math.round(28*28*3.95)
                },
                "30 ft": {
                    gazebo: 22900,
                    compositedeck: 12600,
                    wooddeck: 9650,
                    pagodaroof: 3625,
                    paintedwhite: 5865,
                    woodfinish: 3135,
                    screenpackage: 5950,
                    ezebreeze: 9385,
                    turnedposts: 695,
                    turnedspindles: 2085,
                    wavedcornice: 1350,
                    cedarshingles: Math.round(30*30*3.95)
                },
                victorianbraces: 395,
                electricalpackage: 295
            },
            vinyl: {
                "8 ft": {
                    gazebo: 3800,
                    compositedeck: 960,
                    wooddeck: 795,
                    pagodaroof: "N/A",
                    screenpackage: 930,
                    ezebreeze: 2185,
                    turnedposts: 695,
                    turnedspindles: 350,
                    wavedcornice: 195,
                    cedarshingles: Math.round(8*8*3.95)
                },
                "10 ft": {
                    gazebo: 4650,
                    compositedeck: 1395,
                    wooddeck: 1140,
                    pagodaroof: 925,
                    screenpackage: 1085,
                    ezebreeze: 2685,
                    turnedposts: 695,
                    turnedspindles: 455,
                    wavedcornice: 245,
                    cedarshingles: Math.round(10*10*3.95)
                },
                "12 ft": {
                    gazebo: 5800,
                    compositedeck: 2050,
                    wooddeck: 1560,
                    pagodaroof: 1075,
                    screenpackage: 1295,
                    ezebreeze: 3285,
                    turnedposts: 695,
                    turnedspindles: 580,
                    wavedcornice: 295,
                    cedarshingles: Math.round(12*12*3.95)
                },
                "14 ft": {
                    gazebo: 7100,
                    compositedeck: 2800,
                    wooddeck: 2095,
                    pagodaroof: 1250,
                    screenpackage: 1520,
                    ezebreeze: 3885,
                    turnedposts: 695,
                    turnedspindles: 730,
                    wavedcornice: 360,
                    cedarshingles: Math.round(14*14*3.95)
                },
                "16 ft": {
                    gazebo: 9150,
                    compositedeck: 3670,
                    pagodaroof: 1400,
                    wooddeck: 2760,
                    screenpackage: 1895,
                    ezebreeze: 4585,
                    turnedposts: "N/A",
                    turnedspindles: 920,
                    wavedcornice: 475,
                    cedarshingles: Math.round(16*16*3.95)
                },
                "18 ft": {
                    gazebo: 11450,
                    compositedeck: 4530,
                    wooddeck: 3500,
                    pagodaroof: 1750,
                    screenpackage: 2375,
                    ezebreeze: 5485,
                    turnedposts: "N/A",
                    turnedspindles: 1140,
                    wavedcornice: 595,
                    cedarshingles: Math.round(18*18*3.95)
                },
                "20 ft": {
                    gazebo: 13900,
                    compositedeck: 5615,
                    wooddeck: 4280,
                    pagodaroof: 2275,
                    screenpackage: 2850,
                    ezebreeze: 6385,
                    turnedposts: "N/A",
                    turnedspindles: 1350,
                    wavedcornice: 795,
                    cedarshingles: Math.round(20*20*3.95)
                },
                "24 ft": {
                    gazebo: 19850,
                    compositedeck: 8100,
                    wooddeck: 6225,
                    pagodaroof: 3300,
                    screenpackage: 3850,
                    ezebreeze: 7385,
                    turnedposts: "N/A",
                    turnedspindles: 1570,
                    wavedcornice: 995,
                    cedarshingles: Math.round(24*24*3.95)
                },
                "28 ft": {
                    gazebo: 25800,
                    compositedeck: 11120,
                    wooddeck: 8600,
                    pagodaroof: 4075,
                    screenpackage: 5185,
                    ezebreeze: 8385,
                    turnedposts: "N/A",
                    turnedspindles: 1850,
                    wavedcornice: 1195,
                    cedarshingles: Math.round(28*28*3.95)
                },
                "30 ft": {
                    gazebo: 29550,
                    compositedeck: 12600,
                    wooddeck: 9650,
                    pagodaroof: 4675,
                    screenpackage: 5950,
                    ezebreeze: 9385,
                    turnedposts: "N/A",
                    turnedspindles: 2085,
                    wavedcornice: 1395,
                    cedarshingles: Math.round(30*30*3.95)
                },
                victorianbraces: 495,
                electricalpackage: 295
            }

        },
        ovalrectangle: {
            wood: {
                "10x10":{
                    gazebo: 3800,
                    compositedeck: 1765,
                    wooddeck: 1295,
                    pagodaroof: 975,
                    paintedwhite: 1115,
                    woodfinish: 810,
                    screenpackage: 1255,
                    ezebreeze: 3200,
                    turnedposts: 395,
                    turnedspindles: 330,
                    victorianbraces: 495,
                    cedarshingles: Math.round(10*10*3.95),
                    wavedcornice: 260
                },
                "10x12":{
                    gazebo: 4200,
                    compositedeck: 1765,
                    wooddeck: 1295,
                    pagodaroof: 975,
                    paintedwhite: 1115,
                    woodfinish: 810,
                    screenpackage: 1255,
                    ezebreeze: 3200,
                    turnedposts: 395,
                    turnedspindles: 330,
                    victorianbraces: 495,
                    cedarshingles: Math.round(10*12*3.95),
                    wavedcornice: 260
                },
                "10x14":{
                    gazebo: 4800,
                    compositedeck: 2060,
                    wooddeck: 1520,
                    pagodaroof: 1250,
                    paintedwhite: 1350,
                    woodfinish: 930,
                    screenpackage: 1360,
                    ezebreeze: 3750,
                    turnedposts: 395,
                    turnedspindles: 385,
                    victorianbraces: 495,
                    cedarshingles: Math.round(10*14*3.95),
                    wavedcornice: 275
                },
                "10x16":{
                    gazebo: 5400,
                    compositedeck: 2350,
                    wooddeck: 1730,
                    pagodaroof: 1450,
                    paintedwhite: 1525,
                    woodfinish: 1040,
                    screenpackage: 1465,
                    ezebreeze: 4350,
                    turnedposts: 395,
                    turnedspindles: 440,
                    victorianbraces: 495,
                    cedarshingles: Math.round(10*16*3.95),
                    wavedcornice: 310
                },
                "10x20":{
                    gazebo: 6600,
                    compositedeck: 2940,
                    wooddeck: 2095,
                    pagodaroof: 1850,
                    paintedwhite: 1885,
                    woodfinish: 1210,
                    screenpackage: 1730,
                    ezebreeze: 5450,
                    turnedposts: 395,
                    turnedspindles: 500,
                    victorianbraces: 495,
                    cedarshingles: Math.round(10*20*3.95),
                    wavedcornice: 340
                },
                "12x12":{
                    gazebo: 4500,
                    compositedeck: 2060,
                    wooddeck: 1520,
                    pagodaroof: 1250,
                    paintedwhite: 1350,
                    woodfinish: 930,
                    screenpackage: 1360,
                    ezebreeze: 3750,
                    turnedposts: 395,
                    turnedspindles: 385,
                    victorianbraces: 495,
                    cedarshingles: Math.round(12*12*3.95),
                    wavedcornice: 275
                },
                "12x14":{
                    gazebo: 5300,
                    compositedeck: 2470,
                    wooddeck: 1825,
                    pagodaroof: 1350,
                    paintedwhite: 1525,
                    woodfinish: 1040,
                    screenpackage: 1575,
                    ezebreeze: 4350,
                    turnedposts: 395,
                    turnedspindles: 500,
                    victorianbraces: 495,
                    cedarshingles: Math.round(12*14*3.95),
                    wavedcornice: 300
                },
                "12x16":{
                    gazebo: 6000,
                    compositedeck: 2820,
                    wooddeck: 2080,
                    pagodaroof: 1550,
                    paintedwhite: 1780,
                    woodfinish: 1150,
                    screenpackage: 1765,
                    ezebreeze: 4900,
                    turnedposts: 395,
                    turnedspindles: 500,
                    victorianbraces: 495,
                    cedarshingles: Math.round(12*16*3.95),
                    wavedcornice: 350
                },
                "12x18":{
                    gazebo: 6700,
                    compositedeck: 3150,
                    wooddeck: 2350,
                    pagodaroof: 1850,
                    paintedwhite: 1990,
                    woodfinish: 1250,
                    screenpackage: 2050,
                    ezebreeze: 5450,
                    turnedposts: 395,
                    turnedspindles: 550,
                    victorianbraces: 495,
                    cedarshingles: Math.round(12*18*3.95),
                    wavedcornice: 385
                },
                "12x20":{
                    gazebo: 7400,
                    compositedeck: 3530,
                    wooddeck: 2600,
                    pagodaroof: 2050,
                    paintedwhite: 2240,
                    woodfinish: 1390,
                    screenpackage: 2200,
                    ezebreeze: 6000,
                    turnedposts: 495,
                    turnedspindles: 610,
                    victorianbraces: 595,
                    cedarshingles: Math.round(12*20*3.95),
                    wavedcornice: 440
                },
                "12x24":{
                    gazebo: 8800,
                    compositedeck: 4200,
                    wooddeck: 3120,
                    pagodaroof: 2350,
                    paintedwhite: 2560,
                    woodfinish: 1600,
                    screenpackage: 2620,
                    ezebreeze: 7100,
                    turnedposts: 495,
                    turnedspindles: 715,
                    victorianbraces: 595,
                    cedarshingles: Math.round(12*24*3.95),
                    wavedcornice: 510
                },
                "14x14":{
                    gazebo: 5550,
                    compositedeck: 2820,
                    wooddeck: 2080,
                    pagodaroof: 1550,
                    paintedwhite: 1780,
                    woodfinish: 1150,
                    screenpackage: 1765,
                    ezebreeze: 4900,
                    turnedposts: 395,
                    turnedspindles: 500,
                    victorianbraces: 495,
                    cedarshingles: Math.round(14*14*3.95),
                    wavedcornice: 350
                },
                "14x18":{
                    gazebo: 7900,
                    compositedeck: 3660,
                    wooddeck: 2725,
                    pagodaroof: 2200,
                    paintedwhite: 2300,
                    woodfinish: 1500,
                    screenpackage: 2300,
                    ezebreeze: 6000,
                    turnedposts: 595,
                    turnedspindles: 610,
                    victorianbraces: 695,
                    cedarshingles: Math.round(14*18*3.95),
                    wavedcornice: 430
                },
                "14x20":{
                    gazebo: 8700,
                    compositedeck: 4090,
                    wooddeck: 3030,
                    pagodaroof: 2500,
                    paintedwhite: 2600,
                    woodfinish: 1700,
                    screenpackage: 2550,
                    ezebreeze: 6600,
                    turnedposts: 595,
                    turnedspindles: 660,
                    victorianbraces: 695,
                    cedarshingles: Math.round(14*20*3.95),
                    wavedcornice: 485
                },
                "14x24":{
                    gazebo: 10300,
                    compositedeck: 4920,
                    wooddeck: 3630,
                    pagodaroof: 3000,
                    paintedwhite: 3135,
                    woodfinish: 2200,
                    screenpackage: 2785,
                    ezebreeze: 7700,
                    turnedposts: 595,
                    turnedspindles: 770,
                    victorianbraces: 695,
                    cedarshingles: Math.round(14*24*3.95),
                    wavedcornice: 540
                },
                "14x28":{
                    gazebo: 11900,
                    compositedeck: 5760,
                    wooddeck: 4195,
                    pagodaroof: 3400,
                    paintedwhite: 3730,
                    woodfinish: 2650,
                    screenpackage: 3030,
                    ezebreeze: 8250,
                    turnedposts: 685,
                    turnedspindles: 880,
                    victorianbraces: 795,
                    cedarshingles: Math.round(14*28*3.95),
                    wavedcornice: 620
                },
                electricalpackage: 295
            },
            vinyl: {
                "10x10": {
                    gazebo: 5025,
                    compositedeck: 1765,
                    wooddeck: 1295,
                    pagodaroof: 1150,
                    screenpackage: 1255,
                    turnedposts: 995,
                    turnedspindles: 330,
                    victorianbraces: 595,
                    ezebreeze: 3200,
                    cedarshingles: Math.round(10*10*3.95),
                    wavedcornice: 315
                },
                "10x12": {
                    gazebo: 5400,
                    compositedeck: 1765,
                    wooddeck: 1295,
                    pagodaroof: 1150,
                    screenpackage: 1255,
                    turnedposts: 995,
                    turnedspindles: 330,
                    victorianbraces: 595,
                    ezebreeze: 3200,
                    cedarshingles: Math.round(10*12*3.95),
                    wavedcornice: 355
                },
                "10x14": {
                    gazebo: 6100,
                    compositedeck: 2060,
                    wooddeck: 1520,
                    pagodaroof: 1350,
                    screenpackage: 1360,
                    turnedposts: 995,
                    turnedspindles: 385,
                    victorianbraces: 595,
                    ezebreeze: 3750,
                    cedarshingles: Math.round(10*14*3.95),
                    wavedcornice: 420
                },
                "10x16": {
                    gazebo: 6800,
                    compositedeck: 2350,
                    wooddeck: 1730,
                    pagodaroof: 1550,
                    screenpackage: 1465,
                    turnedposts: 995,
                    turnedspindles: 440,
                    victorianbraces: 595,
                    ezebreeze: 4350,
                    cedarshingles: Math.round(10*16*3.95),
                    wavedcornice: 420
                },
                "10x20": {
                    gazebo: 8200,
                    compositedeck: 2940,
                    wooddeck: 2095,
                    pagodaroof: 1990,
                    screenpackage: 1730,
                    turnedposts: 995,
                    turnedspindles: 500,
                    victorianbraces: 595,
                    ezebreeze: 5450,
                    cedarshingles: Math.round(10*20*3.95),
                    wavedcornice: 470
                },
                "12x12": {
                    gazebo: 5950,
                    compositedeck: 2060,
                    wooddeck: 1520,
                    pagodaroof: 1350,
                    screenpackage: 1360,
                    turnedposts: 995,
                    turnedspindles: 385,
                    victorianbraces: 595,
                    ezebreeze: 3750,
                    cedarshingles: Math.round(12*12*3.95),
                    wavedcornice: 355
                },
                "12x14": {
                    gazebo: 6900,
                    compositedeck: 2470,
                    wooddeck: 1825,
                    pagodaroof: 1550,
                    screenpackage: 1575,
                    turnedposts: 995,
                    turnedspindles: 440,
                    victorianbraces: 595,
                    ezebreeze: 4350,
                    cedarshingles: Math.round(12*14*3.95),
                    wavedcornice: 415
                },
                "12x16": {
                    gazebo: 7800,
                    compositedeck: 2820,
                    wooddeck: 2080,
                    pagodaroof: 1850,
                    screenpackage: 1765,
                    turnedposts: 995,
                    turnedspindles: 500,
                    victorianbraces: 595,
                    ezebreeze: 4900,
                    cedarshingles: Math.round(12*16*3.95),
                    wavedcornice: 470
                },
                "12x18": {
                    gazebo: 8700,
                    compositedeck: 3150,
                    wooddeck: 2350,
                    pagodaroof: 2150,
                    screenpackage: 2050,
                    turnedposts: 995,
                    turnedspindles: 550,
                    victorianbraces: 595,
                    ezebreeze: 5450,
                    cedarshingles: Math.round(12*18*3.95),
                    wavedcornice: 535
                },
                "12x20": {
                    gazebo: 9600,
                    compositedeck: 3530,
                    wooddeck: 2600,
                    pagodaroof: 2450,
                    screenpackage: 2200,
                    turnedposts: 1195,
                    turnedspindles: 610,
                    victorianbraces: 740,
                    ezebreeze: 6000,
                    cedarshingles: Math.round(12*20*3.95),
                    wavedcornice: 605
                },
                "12x24": {
                    gazebo: 11400,
                    compositedeck: 4200,
                    wooddeck: 3120,
                    pagodaroof: 3050,
                    screenpackage: 2620,
                    turnedposts: 1195,
                    turnedspindles: 715,
                    victorianbraces: 740,
                    ezebreeze: 7100,
                    cedarshingles: Math.round(12*24*3.95),
                    wavedcornice: 650
                },
                "14x14": {
                    gazebo: 7450,
                    compositedeck: 2820,
                    wooddeck: 2080,
                    pagodaroof: 1850,
                    screenpackage: 1765,
                    turnedposts: 995,
                    turnedspindles: 500,
                    victorianbraces: 595,
                    ezebreeze: 4900,
                    cedarshingles: Math.round(14*14*3.95),
                    wavedcornice: 420
                },
                "14x18": {
                    gazebo: 10400,
                    compositedeck: 3660,
                    wooddeck: 2725,
                    pagodaroof: 2500,
                    screenpackage: 2300,
                    turnedposts: 1395,
                    turnedspindles: 610,
                    victorianbraces: 880,
                    ezebreeze: 6000,
                    cedarshingles: Math.round(14*18*3.95),
                    wavedcornice: 585
                },
                "14x20": {
                    gazebo: 11500,
                    compositedeck: 4090,
                    wooddeck: 3030,
                    pagodaroof: 2800,
                    screenpackage: 2550,
                    turnedposts: 1395,
                    turnedspindles: 660,
                    victorianbraces: 880,
                    ezebreeze: 6600,
                    cedarshingles: Math.round(14*20*3.95),
                    wavedcornice: 640
                },
                "14x24": {
                    gazebo: 13700,
                    compositedeck: 4920,
                    wooddeck: 3630,
                    pagodaroof: 3400,
                    screenpackage: 2785,
                    turnedposts: 1395,
                    turnedspindles: 770,
                    victorianbraces: 880,
                    ezebreeze: 7700,
                    cedarshingles: Math.round(14*24*3.95),
                    wavedcornice: 815
                },
                "14x28": {
                    gazebo: 15900,
                    compositedeck: 5760,
                    wooddeck: 4195,
                    pagodaroof: 3900,
                    screenpackage: 3030,
                    turnedposts: 1595,
                    turnedspindles: 880,
                    victorianbraces: 995,
                    ezebreeze: 8250,
                    cedarshingles: Math.round(14*28*3.95),
                    wavedcornice: 910
                },
                electricalpackage: 295
            }
        }
}

function step3price(){
    $("#step3_price_wood").text("$" + price[pricebase]["wood"][gsize].gazebo);
    $("#step3_price_vinyl").text("$" + price[pricebase]["vinyl"][gsize].gazebo);
}

function step4price(){
    if(price[pricebase][pricematerial][gsize].pagodaroof != "N/A") {
        $("#step4_price_pagoda").text("+$" + price[pricebase][pricematerial][gsize].pagodaroof);
    } else {
        $("#step4_price_pagoda").text("N/A");
    }

    finalprice.f_gazebo = price[pricebase][pricematerial][gsize].gazebo;
}

function step5price(){
//    if(pricematerial === "wood"){
        $("#step5_price_cedarwood").text("+$" + price[pricebase]["wood"][gsize].cedarshingles);
//    }
}

function step6price(){
    $("#step6_price_waved").text("+$" + price[pricebase][pricematerial][gsize].wavedcornice)
}

function step7price(){
    if(price[pricebase][pricematerial][gsize].turnedposts != "N/A"){
        $("#step7_price_turned").text("+$" + price[pricebase][pricematerial][gsize].turnedposts);
    } else {
        $("#step7_price_turned").text("N/A");
    }
}

function step8price(){
    if(pricebase === "octagon"){
        $("#step8_price_victorian").text("+$" + price[pricebase][pricematerial].victorianbraces);
    } else {
        $("#step8_price_victorian").text("+$" + price[pricebase][pricematerial][gsize].victorianbraces);
    }
}

function step9price(){
    $("#step9_price_turned").text("+$" + price[pricebase][pricematerial][gsize].turnedspindles);
}

function step10price(){
    if(pricematerial === "wood"){
        $("#step10_price_paintedwhite").text("+$" + price[pricebase][pricematerial][gsize].paintedwhite);
        $(".step10_price_woodfinish").text("+$" + price[pricebase][pricematerial][gsize].woodfinish);
    } else{
        $("#step10_price_ivory").text("+$" + Math.round(price[pricebase]["vinyl"][gsize].gazebo * 0.1) );
    }
}

function step11price(){
    $(".step11_price_composite").text("+$" + price[pricebase][pricematerial][gsize].compositedeck );
    $("#step11_price_wood").text("+$" + price[pricebase][pricematerial][gsize].wooddeck );
    $("#step11_price_nodeck").text("");
}

function step12price(){
    $("#step12_price_basic").text("+$" + price[pricebase][pricematerial][gsize].screenpackage );
    $(".step12_price_ezebreeze").text("+$" + price[pricebase][pricematerial][gsize].ezebreeze );
    $("#step12_price_electrical").text("+$"+ price[pricebase][pricematerial].electricalpackage)
}

function summaryRefresh(){
    summary_text(3,"Gazebo Material: "+$("#step3 .designer-image-option.active").text().trim());
    if($("#step4 .designer-image-option").hasClass("active"))
        summary_text(4,"Roof Style: "+$("#step4 .designer-image-option.active").text().trim());
    if($("#step5 li").hasClass("active")){

        if((pricematerial === "vinyl") && $("#cedarwood").hasClass("active")){
            $("#cedarwood").removeClass("active");
            finalprice.f_shingles = 0;
            summary_text(5,"Shingle Color: Select Color");
        } else {
            summary_text(5,"Shingle Color: "+$("#step5 li.active").text().trim());
        }
    } else {
        finalprice.f_shingles = 0;
    }

    if($("#step6 .designer-image-option").hasClass("active")){
        summary_text(6,"Cornice Style: "+$("#step6 .designer-image-option.active").text().trim());
    }

    if($("#step7 .designer-image-option").hasClass("active")){
        summary_text(7,"Style of Posts: "+$("#step7 .designer-image-option.active").text().trim());
    }

    if($("#step8 .designer-image-option").hasClass("active")){
        summary_text(8,"Style of Braces: "+$("#step8 .designer-image-option.active").text().trim());
    }

    if($("#step9 .designer-image-option").hasClass("active")){
        summary_text(9,"Style of Spindle: "+$("#step9 .designer-image-option.active").text().trim());
    }

    if($("#step10 li").hasClass("active")){
        summary_text(10,"Color: "+$("#step10 li.active").text().trim());
    }

    if($("#step11 li").hasClass("active")){
        summary_text(11,"Floorboard: "+$("#step11 li.active").text().trim());
    }

    if($("step12a").find(".active")){
        summary_text("12a", "Screen Package: "+$("#step12a .active").text().trim())
    }

    if($("step12b").find(".active")){
        summary_text("12b", "Electrical Package: "+$("#step12b .active").text().trim())
    }

}

function priceRefresh(){

    step3price();
    step4price();
    step5price();
    step6price();
    step7price();
    step8price();
    step9price();
    step10price();
    step11price();
    step12price();

    summaryRefresh();

    $("#estimatedprice").text("$"+finalprice.getPrice());
}

/*
var price = function(){
    this.braces = 0;
    this.cornice = 0;
    this.deck = 0;
    this.electrical = 0;
    this.floorboard = 0;
    this.posts = 0;
    this.railing = 0;
    this.roof = 0;
    this.screenpackage = 0;
}

price.prototype.getPrice = function(){
    var sum = 0;
    for(prop in this){
        if(!isNaN(this[prop]))
            sum += parseFloat(this[prop]);
    }
    return sum;
}

price.prototype.setPrice = function(pricename,newprice){
    if(newprice == "N/A"){
        this[pricename] = "N/A";
    }
    else {
        this[pricename] = parseFloat(newprice);
    }
}

price.prototype.showPrice = function(){
    $("#estimatedprice").text("$"+this.getPrice());
    console.log("price:" + this.getPrice())
}

var prices = new price();


console.log(prices);
console.log(prices.getPrice());

*/

// controls

/// avoid moving the page to the top when clicking on buttons
$("#designer-wrap li a,#designer-wrap dl a,.designer-next,.designer-prev").click(function(e){
    e.preventDefault();
});

/// next & prev buttons
$(".designer-next").click(function () {
  $(this).closest(".designer-slide").stop().fadeOut(function() {
    $(this).next().fadeIn();
  });
});
$(".designer-prev").click(function () {
  $(this).closest(".designer-slide").stop().fadeOut(function() {
    $(this).prev().fadeIn();
  });
});

// controls end

// Summary Text //

function summary_text(step,summary){
    $("#step"+step+"summary").fadeOut('slow',function(){
        $("#step"+step+"summary").html(summary).fadeIn("slow");
    });
}

// Steps

var step1li = $("#step1 ul li a");
$("#noshape").hide();

$(step1li).click(function(){
    $(step1li).parent().removeClass("active");
    $(this).parent().addClass("active");
    $(".gazebo-sizes").hide();
    switch(this.id){
        case 'oval':
            $("#oval-sizes").show();
            $("#noshape").fadeIn();
            gbase = "oval";
            pricebase = "ovalrectangle";
            if(basesize == "xl"){
                basesize = "large";
                gsize = "14x28";
                summary_text(2, "Gazebo Size: Large - Select size");
            } else {
                basesize = "small";
                gsize = "10x12";
                summary_text(2, "Gazebo Size: Small - Select size");
            }
            $("#placeholder").attr("src","themes/E-commerceTheme/images/oval/placeholder.jpg");
            break;
        case 'octagon':
            $("#octagon-sizes").show();
            $("#noshape").fadeIn();
            gbase = "octagon";
            pricebase = "octagon";
            if(basesize == "xl"){
                basesize = "large";
                gsize = "30 ft";
                summary_text(2, "Gazebo Size: Large - Select size");
            } else {
                basesize = "small";
                gsize = "8 ft";
                summary_text(2, "Gazebo Size: Small - Select size");
            }
            $("#placeholder").attr("src","themes/E-commerceTheme/images/octagon/placeholder.jpg");
            break;
        case 'rectangle':
            $("#rectangle-sizes").show();
            $("#noshape").fadeIn();
            gbase = "rectangle";
            pricebase = "ovalrectangle";
            basesize = "10x10";
            gsize = "10x12";
            summary_text(2, "Gazebo Size: Small - Select size");

            $("#placeholder").attr("src","themes/E-commerceTheme/images/rectangle/placeholder.jpg");
            break;
        default:
            $("#noshape").fadeOut(); break;
    }
    trimmedtext = $.trim($(this).text());
    summary_text(1,"Gazebo Shape: "+trimmedtext);
    if($("#step2 li").hasClass("active")){
        $("#step2 li.active").removeClass("active");
        // summary_text(2,"Size: Select size at step2");
        $("#noshape").fadeOut();
    }
    if($("#step3 .designer-image-option").hasClass("active")){

    } else {
        $("#placeholder").fadeIn();
    }
    processAll();
    if(pricebase && pricematerial && gsize)
        priceRefresh();
});

var step2li = $("#step2 ul li a");

$(step2li).click(function(){
    $(step2li).parent().removeClass("active");
    $(this).parent().addClass("active");
    trimmedtext = $.trim($(this).text());
    summary_text(2,"Gazebo Size: "+trimmedtext);
    $("#noshape").fadeIn();
    chooseSize(trimmedtext);
    processAll();
    if(pricebase && pricematerial && gsize)
        priceRefresh();

    if(pricebase && gsize)
        step3price();
});

var step3imageoptions = $("#step3 .designer-image-option");

$(step3imageoptions).click(function(){
    $(step3imageoptions).removeClass("active");
    $(step3imageoptions).stop().animate({ opacity: 0.5});
    $(this).stop().animate({ opacity: 1});
    $(this).addClass("active");
    $(".gazebo-colors").hide();
    // summary_text(3,"Gazebo Material: "+$.trim($(this).text()));
    switch($(this).attr("id")){
        case 'wood':
            gmat = "wood";
            deckmat = "wood";
            gbasemat = "charc";
            pricematerial = "wood";
            $("#wood-colors").show();
            if($("#deck").is(":visible")){
                processAll();
            } else if(floorboard == "nodeck") {
                processAll();
            } else{
                process("deck");
            }

            if($("#step10summary").is(":visible")){
                summary_text(10,"Stain Color: Wood");
                $("#step10 ul li").removeClass("active");
            }

            //$(".woodshingles").fadeIn();
            break;
        case 'vinyl':
            gmat = "vinyl";
            deckmat = "vinyl";
            gbasemat = "vinyl";
            pricematerial = "vinyl";
            $("#vinyl-colors").show();
            if($("#deck").is(":visible")){
                processAll();
            } else if(floorboard == "nodeck") {
                processAll();
            } else{
                process("deck");
            }

            if($("#step10summary").is(":visible")){
                summary_text(10,"Stain Color: Vinyl");
                $("#step10 ul li").removeClass("active");
            }
            //$(".woodshingles").fadeOut();
            break;
        default: $("no-colors").show(); break;
    }

    if(pricebase && pricematerial && gsize)
        priceRefresh();
    $("#placeholder").fadeOut();

});

var step4imageoptions = $("#step4 .designer-image-option");

$(step4imageoptions).click(function(){
    $(step4imageoptions).removeClass("active");
    $(step4imageoptions).stop().animate({ opacity: 0.5});
    $(this).stop().animate({ opacity: 1});
    $(this).addClass("active");
    summary_text(4,"Roof Style: "+$.trim($(this).text()));
    roof = $("#step4 .active").attr("id");
    if($("#roof").attr("src")){
        process("roof");
        if($("#electrical").attr("src")){
            process("electrical");
        }
        if($("#shingles").attr("src")){
            process("shingles");
        }
    } else {
        process("roof");
    }
});

var step5li = $("#step5 ul li a");
$(step5li).click(function(){
    $(step5li).parent().removeClass("active");
    $(this).parent().addClass("active");
    summary_text(5,"Shingle Color: "+$.trim($(this).text()));
    shingles = $("#step5 .active").attr("id");
    process("shingles");
});

var step6imageoptions = $("#step6 .designer-image-option");
$(step6imageoptions).click(function(){
    $(step6imageoptions).removeClass("active");
    $(step6imageoptions).stop().animate({ opacity: 0.5});
    $(this).stop().animate({ opacity: 1});
    $(this).addClass("active");
    cutout = $(this).attr("id").substring(0,$(this).attr("id").indexOf("-"));
    cornice = cutout;
    process("cornice");
    summary_text(6,"Cornice Style: "+$.trim($(this).text()));
});

var step7imageoptions = $("#step7 .designer-image-option");
$(step7imageoptions).click(function(){
    $(step7imageoptions).removeClass("active");
    $(step7imageoptions).stop().animate({ opacity: 0.5});
    $(this).stop().animate({ opacity: 1});
    $(this).addClass("active");
    cutout = $(this).attr("id").substring(0,$(this).attr("id").indexOf("-"));
    posts = cutout;
    process("posts");
    if($("#railings").attr("src")){
        process("railings");
    }
        if(screenpackage == "screen"){
            process("screenpackage");
        }
    summary_text(7,"Style of Posts: "+$.trim($(this).text()));
});

var step8imageoptions = $("#step8 .designer-image-option");
$(step8imageoptions).click(function(){
    $(step8imageoptions).removeClass("active");
    $(step8imageoptions).stop().animate({ opacity: 0.5});
    $(this).stop().animate({ opacity: 1});
    $(this).addClass("active");
    cutout = $(this).attr("id").substring(0,$(this).attr("id").indexOf("-"));
    braces = cutout;
    process("braces");
        if((screenpackage == "screen") || (screenpackage == "ezebreeze")){
            process("screenpackage");
        }
    summary_text(8,"Style of Braces: "+$.trim($(this).text()));
});

var step9imageoptions = $("#step9 .designer-image-option");
$(step9imageoptions).click(function(){
    $(step9imageoptions).removeClass("active");
    $(step9imageoptions).stop().animate({ opacity: 0.5});
    $(this).stop().animate({ opacity: 1});
    $(this).addClass("active");
    cutout = $(this).attr("id").substring(0,$(this).attr("id").indexOf("-"));
    railings = cutout;
    process("railings");
        if(screenpackage == "screen"){
            process("screenpackage");
        }
    summary_text(9,"Style of Spindle: "+$.trim($(this).text()));
});

var step10li = $("#step10 ul li a");

$(step10li).click(function(){
    $(step10li).parent().removeClass("active");
    $(this).parent().addClass("active");
    gmat = $(this).parent().attr("id").substring(0,$(this).parent().attr("id").indexOf("-"));
    if(gmat == "vinyl"){
        gbasemat = "vinyl";
        deckmat = "vinyl";
    } else if(gmat == "ivory"){
        gbasemat = "ivory";
        deckmat = "ivory";
    } else {
        gbasemat = "charc";
        deckmat = "wood";
    }
    summary_text(10,"Color: "+$.trim($(this).text()));
    processAll();
});

var step11ali = $("#step11a ul li a");

$(step11ali).click(function(){
    $(step11ali).parent().removeClass("active");
    $(this).parent().addClass("active");
    cutout = $(this).parent().attr("id").substring(0,$(this).parent().attr("id").indexOf("-"));
    floorboard = cutout;
    if(!(floorboard == "nodeck")){
        process("deck");
    }
    process("floorboard");
    summary_text("11a","Floorboard: "+$.trim($(this).text()));
});

var step12ali = $("#step12a ul li a");

$(step12ali).click(function(){
    $(step12ali).parent().removeClass("active");
    $(this).parent().addClass("active");
    screenpackage = $("#step12a .active").attr("id");
    process("screenpackage");
    //if(!(screenpackage == "ezebreeze")){
        summary_text("12a","Screen Package: "+$.trim($(this).text()));
    //} else{
    //    summary_text("12a","Screen Package: Eze-Breeze");
   // }
});

var dldt = $("#step12a dl dt a");

$(dldt).click(function(){
    $(dldt).parent().removeClass("active");
    $(this).parent().addClass("active");
    screenpackage = "ezebreeze";
    ezebreeze = $("#step12a dl dt.active a").attr("id");
    process("screenpackage");
    //summary_text("12a","Screen Package: Eze-Breeze " + $.trim($(this).text()))
	summary_text("12a","Screen Package: Eze-Breeze +" + price[pricebase][pricematerial][gsize].ezebreeze + " " + $.trim($(this).text()));
});

var step12bli = $("#step12b ul li a");

$(step12bli).click(function(){
    $(step12bli).parent().removeClass("active");
    $(this).parent().addClass("active");
    cutout = $(this).attr("id").substring(0,$(this).attr("id").indexOf("-"));
    electrical = cutout;
    process("electrical");
    summary_text("12b","Electrical Package: "+$.trim($(this).text()));
});

function paint(imgid,category){
    $("."+category).fadeOut();
    $("#"+imgid).fadeIn();
}

$("#step12 .designer-next").click(function(){
    $("#designer-summary p").css("opacity","1");
    $("#designer-summary").fadeOut();

    $("#reviewsummary").html($("#designer-summary-list").html());
    $("#reviewsummary p").css("opacity","1");

    $("#review-details").fadeIn();
})

$("#finish .designer-prev").bind("click",function(){
    $("#designer-summary-list").html($("#reviewsummary").html());
    $("#designer-summary").fadeIn();
    $("#reviewsummary").html("");
    $("#review-details").fadeOut();
});

$("#finish .designer-next").bind("click",function(){
    $("#csummary").val($("#reviewsummary").html());
    $("#review-details").fadeOut();
});

$("#contact .designer-prev").bind("click",function(){
    $("#review-details").fadeIn();
});



function chooseSize(sizetext){

    gsize = sizetext;
    if(gbase == "octagon"){

        var numsize = sizetext.substring(0,sizetext.indexOf(" "));
        deck = numsize;

        if(numsize < 12){
            basesize = "small";
        } else if((numsize >= 12) && (numsize < 18)){
            basesize = "med";
        } else if(numsize >= 18){
            basesize = "large";
        }
    } else if(gbase == "rectangle"){
        if((sizetext == "10x10") || (sizetext == "10x12") || (sizetext == "12x12")){
            basesize = "10x10";
            deck = 10;
        } else if(sizetext == "14x14"){
            basesize = "14x14";
            deck = 10;
        } else if((sizetext == "10x20") || (sizetext == "12x18") || (sizetext == "12x20")){
            basesize = "10x20";
            deck = 16;
        } else if((sizetext == "10x14") || (sizetext == "10x16") || (sizetext == "12x14") || (sizetext == "12x16")){
            basesize = "small";
            deck = 10;
        } else if(sizetext == "12x24"){
            basesize = "12x24";
            deck = 16;
        } else if((sizetext == "14x18") || (sizetext == "14x20")) {
            basesize = "14x18";
            deck = 16;
        } else if(sizetext == "14x24"){
            basesize = "14x24";
            deck = 16;
        } else if(sizetext == "14x28") {
            if(gbase == "rectangle"){
                basesize = "xl";
            } else{
                basesize = "large";
            }
            deck = 30;
        }
    } else if(gbase == "oval"){
        if((sizetext == "10x10") || (sizetext == "12x12") || (sizetext == "14x14")){
            basesize = "small";
            deck = 10;
        } else if(sizetext == "10x12"){
            basesize = "10x12";
            deck = 12;
        } else if((sizetext == "10x14") || (sizetext == "10x16") || (sizetext == "12x14") || (sizetext == "12x16") || (sizetext == "12x18") || (sizetext == "14x18")){
            basesize = "10x14";
            deck = 14;
        } else if((sizetext == "12x20") || (sizetext == "10x20") || (sizetext == "12x24")) {
            basesize = "med";
            deck = 14;
        } else if((sizetext == "14x20") || (sizetext == "14x24")) {
            basesize = "med";
            deck = 16;
        } else if(sizetext == "14x28") {
            basesize = "large";
            deck = 30;
        }
    }

    finalprice.f_deck = 0;
}

/*
function processPrice(item){
    if(gbase == "octagon" && pricematerial == "wood"){
        switch(item){
            case "straightcornice": prices.setPrice("wavedcornice", 0); break;
            case "straightposts": prices.setPrice("turnedposts", 0); break;
            case "standardbraces": prices.setPrice("victorianbraces", 0); break;
            case "straightbaluster": prices.setPrice("turnedspindles", 0); break;
            case "naturalwood":
                prices.setPrice("woodfinish", 0);
                prices.setPrice("paintedwhite", 0);
                break;
            case "victorianbraces": prices.setPrice("victorianbraces", 395); break;
            case "electricalpackage": prices.setPrice("electricalpackage", 295); break;
            case "noelectricalpackage": prices.setPrice("electricalpackage", 0); break;
            case "additionaloutlets": prices.setPrice("additionaloutlets", 195); break;
            case "custompaintcolor": prices.setPrice("custompaintcolor", 175); break;
            case "cedarshingles": prices.setPrice("cedarshingles", 3.95); break;
            case "fullsizeweathervane": prices.setPrice("fullsizeweathervane", 395); break;
            case "noscreenpackage": prices.setPrice("screenpackage", 0); break;
            default: break;
        }
        switch(gsize){
            case "8 ft":
                switch(item){
                    case "deck": prices.setPrice("deck", 795); break;
                    case "roof": prices.setPrice("roof", "N/A"); break; // N/A
                    case "woodfinish": prices.setPrice("woodfinish", 540); break;
                    case "screenpackage": prices.setPrice("screenpackage", 930); break;
                    case "turnedposts": prices.setPrice("turnedposts", 295); break;
                    case "turnedspindles": prices.setPrice("turnedspindles", 350); break;
                    case "wavedcornice": prices.setPrice("wavedcornice", 195); break;
                    case "benchespersection": prices.setPrice("benchespersection", 80); break;
                    case "onsiteassembly": prices.setPrice("onsiteassembly", 990); break;
                    case "paintedwhite": prices.setPrice("paintedwhite", 865); break;
                    default: break;
                } break;
            case "10 ft":
                switch(item){
                    case "deck": prices.setPrice("deck", 1140); break;
                    case "roof": prices.setPrice("roof", 725); break;
                    case "woodfinish": prices.setPrice("woodfinish", 760); break;
                    case "screenpackage": prices.setPrice("screenpackage", 1085); break;
                    case "turnedposts": prices.setPrice("turnedposts", 295); break;
                    case "turnedspindles": prices.setPrice("turnedspindles", 455); break;
                    case "wavedcornice": prices.setPrice("wavedcornice", 235); break;
                    case "benchespersection": prices.setPrice("benchespersection", 90); break;
                    case "onsiteassembly": prices.setPrice("onsiteassembly", 1430); break;
                    case "paintedwhite": prices.setPrice("paintedwhite", 1100); break;
                    default: break;
                } break;
            case "12 ft": console.log("12 ft yay"); break;
            case "14 ft": console.log("14 ft yay"); break;
            case "16 ft": console.log("16 ft yay"); break;
            case "18 ft": console.log("18 ft yay"); break;
            case "20 ft": console.log("20 ft yay"); break;
            case "24 ft": console.log("24 ft yay"); break;
            case "28 ft": console.log("28 ft yay"); break;
            case "30 ft": console.log("30 ft yay"); break;
            default: break;
        }
    }
*/

   /* if(gggbase == "octagon" && deckmat == "vinyl"){
        switch("gsize"){
            case "8 ft":
                switch(item){
                    case "paintedwhite": prices.setPrice("paintedwhite", 865); break;
                    default: break;
                }
                break;
            case "10 ft":
                switch(item){
                    case "paintedwhite": prices.setPrice("paintedwhite", 1100); break;
                    default: break;
                }
                break;
            default: break;
        }
    }

}*/


function process(categoryid){

$("#"+categoryid).hide();

if(pricebase && pricematerial && gsize){
    if((pricematerial === "vinyl") && (gmat === "ivory")){
        finalprice.f_gazebo = Math.round(price[pricebase][pricematerial][gsize].gazebo * 1.1);
    } else {
        finalprice.f_gazebo = price[pricebase][pricematerial][gsize].gazebo;
    }
}

if(gbase == "octagon"){
    switch(categoryid){
        case "braces":
            $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +".png");
            if(braces == "victorian"){
                finalprice.f_braces = price[pricebase][pricematerial].victorianbraces;
            } else{
                finalprice.f_braces = 0;
            }
            break;
        case "cornice":
            if(cornice == "waved"){
                if(roof == "pagoda"){
                    $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "/" + cornice + "-" + basesize + "-" + gmat + "-pag" + ".png");
                } else if(roof == "majestic"){
                    $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "/" + cornice + "-" + basesize + "-" + gmat + ".png");
                }
                finalprice.f_cornice = price[pricebase][pricematerial][gsize].wavedcornice;
            }
            else if(cornice == "straight"){
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "/" + cornice + "-" + basesize + "-" + gmat + ".png");
                finalprice.f_cornice = 0;
            }
            break;
        case "deck":
            $("#deck").attr("src","themes/E-commerceTheme/images/" + gbase + "/deck/" + deck + "ft/" + deck + "-" + deckmat + ".png");
            break;
        case "floorboard":
            if(floorboard == "wood"){
                $("#floorboard").attr("src","themes/E-commerceTheme/images/" + gbase + "/floorboard/" + deck + "-wood.png");
                $("#deck").fadeIn();
                finalprice.f_deck = price[pricebase][pricematerial][gsize].wooddeck;
            } else if(floorboard == "nodeck"){
                $("#deck").fadeOut();
                finalprice.f_deck = 0;
            } else{
                $("#floorboard").attr("src","themes/E-commerceTheme/images/" + gbase + "/floorboard/" + deck + "-" + floorboard + ".png");
                $("#deck").fadeIn();
                finalprice.f_deck = price[pricebase][pricematerial][gsize].compositedeck;
            }
            break;
        case "posts":
            $("#posts").attr("src","themes/E-commerceTheme/images/" + gbase + "/posts/" + posts + "/post" + posts + "-" + basesize + "-" + gmat + ".png");
            if(posts == "turned"){
               finalprice.f_posts = price[pricebase][pricematerial][gsize].turnedposts;
            } else {
               finalprice.f_posts = 0;
            }
            break;
        case "railings":
                if(posts == "straight"){
                    switch(railings){
                        case "straight":
                            $("#railings").attr("src","themes/E-commerceTheme/images/" + gbase + "/railings/" + railings + "/s-s-" + basesize + "-" + gmat + ".png");
                            finalprice.f_railings = 0;
                            break;
                        case "turned":
                            $("#railings").attr("src","themes/E-commerceTheme/images/" + gbase + "/railings/" + railings + "/s-t-" + basesize + "-" + gmat + ".png");
                            finalprice.f_railings = price[pricebase][pricematerial][gsize].turnedspindles;
                            break;
                    }
                } else if(posts == "turned"){
                    switch(railings){
                        case "straight":
                            $("#railings").attr("src","themes/E-commerceTheme/images/" + gbase + "/railings/" + railings + "/t-s-" + basesize + "-" + gmat + ".png");
                            finalprice.f_railings = 0;
                            break;
                        case "turned":
                            $("#railings").attr("src","themes/E-commerceTheme/images/" + gbase + "/railings/" + railings + "/t-t-" + basesize + "-" + gmat + ".png");
                            finalprice.f_railings = price[pricebase][pricematerial][gsize].turnedspindles;
                            break;
                    }
                }
            break;
        case "roof":
            $("#roof").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/" + roof + "/" + roof + "-base-" + basesize + "-" + gmat + ".png");
            if(roof === "majestic"){
                finalprice.f_roof = 0;
            } else{
                finalprice.f_roof = price[pricebase][pricematerial][gsize].pagodaroof;
            }
            break;
        case "shingles":
            if(shingles === "cedarwood"){
                finalprice.f_shingles = price[pricebase][pricematerial][gsize].cedarshingles;
                $("#shingles").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/" + roof + "/shingles/" + roof + "-" + basesize + "-earthtonecedar.png");
            } else {
                finalprice.f_shingles = 0;
                $("#shingles").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/" + roof + "/shingles/" + roof + "-" + basesize + "-" + shingles + ".png");
            }
            break;
        case "electrical":
            switch(electrical){
                case "yes":
                    $("#electrical").attr("src","themes/E-commerceTheme/images/" + gbase + "/electrical-packages/" + "elec-" + basesize + "-" + roof + ".png");
                    finalprice.f_electrical = price[pricebase][pricematerial].electricalpackage;
                    break;
                case "no":
                    $("#electrical").attr("src","");
                    finalprice.f_electrical = 0;
                    break;
            }
            break;
        case "screenpackage":
            switch(screenpackage){
                case "screen":
                    $("#eze-breeze-wrap").fadeOut();
                    if(railings == "straight"){
                        $("#door").attr("src","themes/E-commerceTheme/images/octagon/screen-package/door/door-str-" + basesize + "-" + gmat + ".png");
                    } else if(railings == "turned"){
                        $("#door").attr("src","themes/E-commerceTheme/images/octagon/screen-package/door/door-turned-" + basesize + "-" + gmat + ".png");
                    }
                    if(braces == "standard"){
                        $("#screentop").attr("src","themes/E-commerceTheme/images/octagon/screen-package/screen/screentop-" + basesize + "-standard.png");
                    } else if(braces == "victorian"){
                        $("#screentop").attr("src","themes/E-commerceTheme/images/octagon/screen-package/screen/screentop-" + basesize + "-victorian.png");
                    }
                    if(posts == "straight"){
                        $("#screenbottom").attr("src","themes/E-commerceTheme/images/octagon/screen-package/screen/screenbottom-" + basesize + "-straight.png");
                    } else if(posts == "turned"){
                        $("#screenbottom").attr("src","themes/E-commerceTheme/images/octagon/screen-package/screen/screenbottom-" + basesize + "-turned.png");
                    }

                    if(basesize == "med"){
                        $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +"-crop.png");
                    } else {
                        $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +".png");
                    }
                    finalprice.f_screen = price[pricebase][pricematerial][gsize].screenpackage;
                    break;
                case "ezebreeze":
                    $("#eze-breeze-wrap").fadeIn();
                    $("#door").attr("src","");
                    $("#screenbottom").attr("src","");
                    $("#screentop").attr("src","");
                    if(railings == "straight"){
                        $("#door").attr("src","themes/E-commerceTheme/images/octagon/screen-package/door/door-str-" + basesize + "-" + gmat + ".png");
                    } else if(railings == "turned"){
                        $("#door").attr("src","themes/E-commerceTheme/images/octagon/screen-package/door/door-turned-" + basesize + "-" + gmat + ".png");
                    }
                    if(braces == "standard"){
                        $("#screentop").attr("src","themes/E-commerceTheme/images/octagon/screen-package/screen/screentop-" + basesize + "-standard-" + ezebreeze + ".png");
                    } else if(braces == "victorian"){
                        $("#screentop").attr("src","themes/E-commerceTheme/images/octagon/screen-package/screen/screentop-" + basesize + "-victorian-" + ezebreeze + ".png");
                    }
                    if(posts == "straight"){
                        $("#screenbottom").attr("src","themes/E-commerceTheme/images/octagon/screen-package/screen/screenbottom-" + basesize + "-straight-" + ezebreeze + ".png");
                    } else if(posts == "turned"){
                        $("#screenbottom").attr("src","themes/E-commerceTheme/images/octagon/screen-package/screen/screenbottom-" + basesize + "-turned-" + ezebreeze + ".png");
                    }

                    if(basesize == "med"){
                        $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +"-crop.png");
                    } else {
                        $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +".png");
                    }
                    finalprice.f_screen = price[pricebase][pricematerial][gsize].ezebreeze;
                    break;
                case "nopackage":
                    $("#eze-breeze-wrap").fadeOut();
                    $("#door").attr("src","");
                    $("#screenbottom").attr("src","");
                    $("#screentop").attr("src","");
                    $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +".png");
                    finalprice.f_screen = 0;
                    break;
            }
            break;
        default: break;
    }
} else if((gbase == "oval") || (gbase == "rectangle")) {
    switch(categoryid){
        case "braces":
            $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +".png");
            if(braces == "victorian"){
                finalprice.f_braces = price[pricebase][pricematerial][gsize].victorianbraces;
            } else{
                finalprice.f_braces = 0;
            }
            break;
        case "cornice":
            if(gbase == "rectangle"){
                if(cornice == "waved"){

            if(roof == "pagoda"){
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "-" + gmat + "-pag" + ".png");
            } else if(roof == "majestic"){
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "-" + gmat + ".png");
            }
            }
            else if(cornice == "straight"){
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "-" + gmat + ".png");
            }
            }

            else if(((gbase == "oval") && (basesize == "med")) || ((gbase == "oval") && (basesize == "large")) || ((gbase == "oval") && (basesize == "10x14"))){
                if(cornice == "waved"){

            if(roof == "pagoda"){
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "-large-" + gmat + "-pag" + ".png");
            } else if(roof == "majestic"){
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "-" + gmat + ".png");
            }
            }
            else if(cornice == "straight"){
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "-" + gmat + ".png");
            }
            }

            else if(((gbase == "oval") && (basesize == "small")) || ((gbase == "oval") && (basesize == "10x12"))){
                if(cornice == "waved"){

            if(roof == "pagoda"){
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "-" + gmat + "-pag" + ".png");
            } else if(roof == "majestic"){
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "-" + gmat + ".png");
            }
            }
            else if(cornice == "straight"){
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "-" + gmat + ".png");
            }
            }

            else{
                $("#cornice").attr("src","themes/E-commerceTheme/images/" + gbase + "/cornice/" + cornice + "-" + gmat + ".png");
            }

            if(cornice === "waved"){
                finalprice.f_cornice = price[pricebase][pricematerial][gsize].wavedcornice;
            } else {
                finalprice.f_cornice = 0;
            }
            break;
        case "deck":
            $("#deck").attr("src","themes/E-commerceTheme/images/" + gbase + "/deck/" + basesize + "/" + basesize + "-" + deckmat + ".png");
            break;
        case "floorboard":
            if(floorboard == "wood"){
                $("#floorboard").attr("src","themes/E-commerceTheme/images/" + gbase + "/floorboard/" + basesize + "-wood.png");
                $("#deck").fadeIn();
                finalprice.f_deck = price[pricebase][pricematerial][gsize].wooddeck;
            } else if(floorboard == "nodeck"){
                $("#deck").fadeOut();
                finalprice.f_deck = 0;
            } else{
                $("#floorboard").attr("src","themes/E-commerceTheme/images/" + gbase + "/floorboard/" + basesize + "-" + floorboard + ".png");
                finalprice.f_deck = price[pricebase][pricematerial][gsize].compositedeck;
            }
            break;
        case "posts":
            $("#posts").attr("src","themes/E-commerceTheme/images/" + gbase + "/posts/" + posts + "/post" + posts + "-" + basesize + "-" + gmat + ".png");
            if(posts == "turned"){
               finalprice.f_posts = price[pricebase][pricematerial][gsize].turnedposts;
            } else {
               finalprice.f_posts = 0;
            }
            break;
        case "railings":
                if(posts == "straight"){
                    switch(railings){
                        case "straight":
                            $("#railings").attr("src","themes/E-commerceTheme/images/" + gbase + "/railings/" + railings + "/" + railings + "-" + basesize + "-" + gmat + ".png");
                            finalprice.f_railings = 0;
                            break;
                        case "turned":
                            $("#railings").attr("src","themes/E-commerceTheme/images/" + gbase + "/railings/" + railings + "/" + railings + "-" + basesize + "-" + gmat + ".png");
                            finalprice.f_railings = price[pricebase][pricematerial][gsize].turnedspindles;
                            break;
                    }
                } else if(posts == "turned"){
                    switch(railings){
                        case "straight":
                            $("#railings").attr("src","themes/E-commerceTheme/images/" + gbase + "/railings/" + railings + "/" + railings + "-" + basesize + "-" + gmat + ".png");
                            finalprice.f_railings = 0;
                            break;
                        case "turned":
                            $("#railings").attr("src","themes/E-commerceTheme/images/" + gbase + "/railings/" + railings + "/" + railings + "-" + basesize + "-" + gmat + ".png");
                            finalprice.f_railings = price[pricebase][pricematerial][gsize].turnedspindles;
                            break;
                    }
                }
            break;
        case "roof":
            if(gbase == "rectangle"){
                $("#roof").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/" + roof + "-" + gmat + ".png");
            } else if( (gbase == "oval") && (basesize == "med")){
                $("#roof").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/" + roof + "/" + roof + "-large-" + gmat + ".png");
            } else{
                $("#roof").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/" + roof + "/" + roof + "-" + basesize + "-" + gmat + ".png");
            }
            if(roof === "majestic"){
                finalprice.f_roof = 0;
            } else{
                finalprice.f_roof = price[pricebase][pricematerial][gsize].pagodaroof;
            }
            break;
        case "shingles":
            if(gbase == "rectangle"){
                if(shingles === "cedarwood"){
                    finalprice.f_shingles = price[pricebase][pricematerial][gsize].cedarshingles;
                    $("#shingles").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/shingles/" + roof + "-earthtonecedar.png");
                } else {
                    $("#shingles").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/shingles/" + roof + "-" + shingles + ".png");
                    finalprice.f_shingles = 0;
                }
            } else if( (gbase == "oval") && (basesize == "med")){
                if(shingles === "cedarwood"){
                    finalprice.f_shingles = price[pricebase][pricematerial][gsize].cedarshingles;
                    $("#shingles").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/" + roof + "/shingles/" + roof + "-large-earthtonecedar.png");
                } else {
                    $("#shingles").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/" + roof + "/shingles/" + roof + "-large-" + shingles + ".png");
                    finalprice.f_shingles = 0;
                }
            } else {
                if(shingles === "cedarwood"){
                    finalprice.f_shingles = price[pricebase][pricematerial][gsize].cedarshingles;
                    $("#shingles").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/" + roof + "/shingles/" + roof + "-" + basesize + "-earthtonecedar.png");
                } else {
                    $("#shingles").attr("src","themes/E-commerceTheme/images/" + gbase + "/roof/" + roof + "/shingles/" + roof + "-" + basesize + "-" + shingles + ".png");
                    finalprice.f_shingles = 0;
                }
            }

            break;
        case "electrical":
            switch(electrical){
                case "yes":
                    if(gbase == "rectangle"){
                        $("#electrical").attr("src","themes/E-commerceTheme/images/" + gbase + "/electrical-packages/" + "elec-" + roof + ".png");
                    } else if( (gbase == "oval") && (basesize == "med")){
                        $("#electrical").attr("src","themes/E-commerceTheme/images/" + gbase + "/electrical-packages/" + "elec-large-" + roof + ".png");
                    } else if( (gbase == "oval") && (basesize == "10x12")){
                       $("#electrical").attr("src","themes/E-commerceTheme/images/" + gbase + "/electrical-packages/" + "elec-small-" + roof + ".png");
                    } else if ( (gbase == "oval") && ((basesize != "med") && (basesize != "10x12") && (basesize != "small"))){
                        $("#electrical").attr("src","themes/E-commerceTheme/images/" + gbase + "/electrical-packages/" + "elec-large-" + roof + ".png");
                    } else{
                        $("#electrical").attr("src","themes/E-commerceTheme/images/" + gbase + "/electrical-packages/" + "elec-" + basesize + "-" + roof + ".png");
                    }
                    finalprice.f_electrical = price[pricebase][pricematerial].electricalpackage;
                    break;
                case "no":
                    $("#electrical").attr("src","");
                    finalprice.f_electrical = 0;
                    break;
            }
            break;
        case "screenpackage":
            switch(screenpackage){
                case "screen":
                    $("#eze-breeze-wrap").fadeOut();
                    if((gbase == "oval") && (basesize == "10x12")){
                        if(railings == "straight"){
                            $("#door").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/door/door-str-" + basesize + "-" + gmat + ".png");
                        } else if(railings == "turned"){
                            $("#door").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/door/door-turned-" + basesize + "-" + gmat + ".png");
                        }
                    } else{
                        if(railings == "straight"){
                            $("#door").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/door/door-str-" + basesize + "-" + gbasemat + ".png");
                        } else if(railings == "turned"){
                            $("#door").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/door/door-turned-" + basesize + "-" + gbasemat + ".png");
                        }
                    }
                    if(braces == "standard"){
                        $("#screentop").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screentop-" + basesize + "-standard.png");
                    } else if(braces == "victorian"){
                        $("#screentop").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screentop-" + basesize + "-victorian.png");
                    }
                    if(posts == "straight"){
                        if((gbase == "rectangle") && (basesize == "small")){
                            $("#screenbottom").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screenbottom-small-straight.png");
                        } else {
                            $("#screenbottom").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screenbottom-" + basesize + ".png");
                        }
                    } else if(posts == "turned"){
                        if((gbase == "rectangle") && (basesize == "small")){
                            $("#screenbottom").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screenbottom-small-turned.png");
                        } else {
                            $("#screenbottom").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screenbottom-" + basesize + ".png");
                        }
                    }

                    if((gbase == "oval") && (basesize == "10x12")){
                        $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +"-crop.png");
                    } else {
                        $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +".png");
                    }
                    finalprice.f_screen = price[pricebase][pricematerial][gsize].screenpackage;
                    break;
                case "ezebreeze":
                    $("#eze-breeze-wrap").fadeIn();
                    $("#door").attr("src","");
                    $("#screenbottom").attr("src","");
                    $("#screentop").attr("src","");

                    if((gbase == "oval") && (basesize == "10x12")){
                        if(railings == "straight"){
                            $("#door").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/door/door-str-" + basesize + "-" + gmat + ".png");
                        } else if(railings == "turned"){
                            $("#door").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/door/door-turned-" + basesize + "-" + gmat + ".png");
                        }
                    } else {
                        if(railings == "straight"){
                            $("#door").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/door/door-str-" + basesize + "-" + gbasemat + ".png");
                        } else if(railings == "turned"){
                            $("#door").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/door/door-turned-" + basesize + "-" + gbasemat + ".png");
                        }
                    }
                    if(braces == "standard"){
                        $("#screentop").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screentop-" + basesize + "-standard-" + ezebreeze + ".png");
                    } else if(braces == "victorian"){
                        $("#screentop").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screentop-" + basesize + "-victorian-" + ezebreeze + ".png");
                    }
                    if(posts == "straight"){
                        if((gbase == "rectangle") && (basesize == "small")){
                            $("#screenbottom").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screenbottom-small-straight-" + ezebreeze + ".png");
                        } else {
                            $("#screenbottom").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screenbottom-" + basesize + "-" + ezebreeze + ".png");
                        }
                    } else if(posts == "turned"){
                        if((gbase == "rectangle") && (basesize == "small")){
                            $("#screenbottom").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screenbottom-small-turned-" + ezebreeze + ".png");
                        } else {
                            $("#screenbottom").attr("src","themes/E-commerceTheme/images/"+ gbase +"/screen-package/screen/screenbottom-" + basesize + "-" + ezebreeze + ".png");
                        }
                    }

                    if((gbase == "oval") && (basesize == "10x12")){
                        $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +"-crop.png");
                    } else {
                        $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +".png");
                    }
                    finalprice.f_screen = price[pricebase][pricematerial][gsize].ezebreeze;
                    break;
                case "nopackage":
                    $("#eze-breeze-wrap").fadeOut();
                    $("#door").attr("src","");
                    $("#screenbottom").attr("src","");
                    $("#screentop").attr("src","");
                    $("#braces").attr("src","themes/E-commerceTheme/images/" + gbase + "/braces/" + braces + "/" + braces + "-" + basesize + "-" + gmat +".png");
                    finalprice.f_screen = 0;
                    break;
            }
            break;
        default: break;
    }
}

    if((categoryid == "floorboard") && (floorboard == "nodeck")){
        $("#floorboard").fadeOut(800);
    } else if((categoryid == "electrical") && (electrical == "no")){
        $("#electrical").fadeOut(800);
    } else if((categoryid == "screenpackage") && (screenpackage == "nopackage")){
        $("#screenbottom").fadeOut(800);
        $("#screentop").fadeOut(800);
        $("#door").fadeOut(800);
    } else if((categoryid == "screenpackage") && ((screenpackage == "screen") || (screenpackage == "ezebreeze"))){
        $("#screenbottom").fadeIn(800);
        $("#screentop").fadeIn(800);
        $("#door").fadeIn(800);
    }
    else{
        $("#"+categoryid).fadeIn(800);
    }

// colors for step10

    if($("#step10 li").hasClass("active")){
        if($("#vinyl-white").hasClass("active")){
            if(pricebase === "vinyl"){
                $("#vinyl-white").removeClass("active")
                finalprice.f_color = 0;
                summary_text(10, "Color: Select Color");
            } else {
                finalprice.f_color = price[pricebase][pricematerial][gsize].paintedwhite;
            }

        } else if($("#wood-natural").hasClass("active")){
            if(pricebase === "vinyl"){
                $("#vinyl-white").removeClass("active")
                finalprice.f_color = 0;
                summary_text(10, "Color: Select Color");
            } else {
                finalprice.f_color = 0;
            }
        } else if($("#woodfinish li").hasClass("active")){
            if(pricebase === "vinyl"){
                $("#vinyl-white").removeClass("active")
                finalprice.f_color = 0;
                summary_text(10, "Color: Select Color");
            } else {
                finalprice.f_color = price[pricebase][pricematerial][gsize].woodfinish;
            }
        }
    }


    $("#estimatedprice").text("$"+finalprice.getPrice());

}

function processAll(){
    $("#designer-preview img").each(function(){
        if($(this).is(":visible")){
            process($(this).attr("id"));
        }
        if((screenpackage == "screen") || (screenpackage == "ezebreeze")){
            process("screenpackage");
        }
    });

    $("#estimatedprice").text("$"+finalprice.getPrice());

}

$("#printsummary").bind("click", function(){
    window.print();
});

$("#csubmit").click(function(){
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    };

    var is_ie = navigator.userAgent.toLowerCase().indexOf('msie') > -1;

    if(is_ie && 1 != 1){
        alert("We're sorry! Your browser is not compatible with our script. You may use another browser, or please call us at 1.800.700.1777 to talk to one of our designers. Thank you!");
    } else {
        if(!($("#cfname").val() == "") && !($("#clname").val() == "") && !($("#cemail").val() == "") && !($("#cphone").val() =="")){
            if(isValidEmailAddress($("#cemail").val())){
				$.support.cors = true;
                $.ajax({
                    type: 'POST',
                    url: "mail-test.php",
                    crossDomain: true,
                    data: {
                        cfname: $("#cfname").val(),
						clname: $("#clname").val(),
                        cemail: $("#cemail").val(),
                        cphone: $("#cphone").val(),
                        csummary: $("#csummary").val()
                    },
                    success: function(){
						/*
                        $("#csubmit").closest(".designer-slide").stop().fadeOut(function(){
                            $("#designer-summary").fadeOut();
                            $(this).next().fadeIn();
                        });
						*/
					
						$("#csummary").val(encodeURIComponent($("#csummary").val()));
						$('#cform').attr('action', 'https://mail.amishgazebos.com/leadgenerator/DesignYourOwnGazeboXML.aspx');
						$("#cform").submit();
						
                    },
                    error: function(){
                        alert("error");
                    }
                });
            } else {
                alert("Please enter a valid e-mail address.")
            }
        } else {
            alert("Please fill out all the fields.");
        }
    }
});

var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
if(is_chrome){
    $("#finish .designer-controls").append("<p style='margin-top: 3px;'>In Chrome expect a slight delay if you've canceled the print dialog</p>");
}

$("#startover").click(function(){
    var yes = confirm("Do you want to start over?");
    if(yes){
        window.location.reload();
    }
})


});