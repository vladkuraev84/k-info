window.onload = function() {
	var icons = {
		
			'icon-twitter' : '&#xe003;',
			'icon-facebook' : '&#xe004;'
		
	};
	$('[class^="icon-"], [class*=" icon-"]').each(function(index, element) {
		var code = $(this).attr('class').match(/icon-[^\s'"]+/);
		$(this).prepend('<span class="icon-toaster">'+icons[code[0]]+'</span>');
	});
	$('select').each(function() {
		$(this).css('width', $(this).width()+24 );
	});
};